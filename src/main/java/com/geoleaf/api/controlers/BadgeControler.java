package com.geoleaf.api.controlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geoleaf.api.beans.web.BadgeVO;
import com.geoleaf.api.beans.web.UtilisateurBadgeVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.services.IBadgeService;
import com.geoleaf.api.services.IUtilisateurBadgeService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class BadgeControler {

	@Autowired
	private IBadgeService badgeService;
	
	@Autowired
	private IUtilisateurBadgeService utilisateurBadgeService;

	/**
	 * Liste de tous les badges
	 * 
	 * @return
	 */
	@RequestMapping(value = "/badges", method = RequestMethod.GET)
	public List<BadgeVO> getAll() {
		return badgeService.findAll();
	}

	/**
	 * @param id
	 * @return BadgeVO par son id
	 */
	@RequestMapping(value = "/badge/id", method = RequestMethod.GET)
	public BadgeVO getById(@RequestParam("id") Integer id) {
		return badgeService.findById(id);
	}

	/**
	 * Enregistre un nouveau badge
	 * 
	 * @param badgeVO
	 */
	@RequestMapping(value = "/badge/add", method = RequestMethod.POST)
	public void add(@RequestBody BadgeVO badgeVO) {
		badgeService.add(badgeVO);
	}
	
	
	@RequestMapping(value = "/badges/utilisateur", method = RequestMethod.GET)
	public List<UtilisateurBadgeVO> getBadgesForUtilisateur(@RequestParam("utilisateur") String utilisateur) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		UtilisateurVO utilisateurVO = gson.fromJson(utilisateur, UtilisateurVO.class);
		
		return utilisateurBadgeService.findByUtilisateur(utilisateurVO);
	}
	
	@RequestMapping(value = "/badge/utilisateur/add", method = RequestMethod.POST)
	public void add(@RequestBody UtilisateurBadgeVO utilisateurBadgeVO) {
		utilisateurBadgeService.add(utilisateurBadgeVO);
	}
}
