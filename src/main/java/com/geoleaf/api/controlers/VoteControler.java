package com.geoleaf.api.controlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geoleaf.api.beans.web.RecensementVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.beans.web.VoteVO;
import com.geoleaf.api.services.IVoteService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class VoteControler {

	@Autowired
	private IVoteService voteService;

	/**
	 * @return {@link List} de tous les votes
	 */
	@RequestMapping(value = "/votes", method = RequestMethod.GET)
	public List<VoteVO> getAll() {
		return voteService.getAll();
	}
	
	@RequestMapping(value = "/vote/utilisateur/for/recensement", method = RequestMethod.GET)
	public int hasVotedFor(@RequestParam("utilisateur") String u, @RequestParam("recensement") String r /*@RequestBody UtilisateurVO utilisateur, RecensementVO recensement*/) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		UtilisateurVO utilisateur = gson.fromJson(u, UtilisateurVO.class);
		RecensementVO recensement = gson.fromJson(r, RecensementVO.class);
		
		return voteService.getVotedValue(utilisateur, recensement);
	}
	
	@RequestMapping(value = "/vote/add", method = RequestMethod.POST)
	public void add(@RequestBody VoteVO voteVO) {
		voteService.add(voteVO);
	}
	
	@RequestMapping(value = "/vote/count/positive/recensement", method = RequestMethod.GET)
	public Integer countPositive(@RequestParam("recensement") String r) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		RecensementVO recensement = gson.fromJson(r, RecensementVO.class);
		return voteService.countPostiveVotesForRecensement(recensement);
	}
	
	@RequestMapping(value = "/vote/count/negative/recensement", method = RequestMethod.GET)
	public Integer countNegative(@RequestParam("recensement") String r) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		RecensementVO recensement = gson.fromJson(r, RecensementVO.class);
		return voteService.countNegativesVotesForRecensement(recensement);
	}
	
	

}