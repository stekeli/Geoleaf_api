package com.geoleaf.api.controlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geoleaf.api.beans.web.AlerteVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.services.IAlerteService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Kevin Wallois
 *
 */
@RestController
public class AlerteControler {

	@Autowired
	private IAlerteService alerteService;

	/**
	 * @return List de toutes les alertes
	 */
	@RequestMapping(value = "/alertes", method = RequestMethod.GET)
	public List<AlerteVO> getAll() {
		return alerteService.findAll();
	}
	
	/**
	 * @return List de toutes les alertes
	 */
	@RequestMapping(value = "/alertes/actif", method = RequestMethod.GET)
	public List<AlerteVO> getByActif() {
		return alerteService.findAll();
	}

	/**
	 * @param id
	 * @return AlerteVO selon son id
	 */
	@RequestMapping(value = "/alerte/id", method = RequestMethod.GET)
	public AlerteVO getById(@RequestParam("id") Integer id) {
		return alerteService.findById(id);
	}


	/**
	 * @param idUtilisateur
	 * @return {@link List} de recencement d un utilisateur
	 */
	@RequestMapping(value = "/alertes/user", method = RequestMethod.GET)
	public List<AlerteVO> getByUtilisateur(@RequestParam("utilisateur") String utilisateur) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		UtilisateurVO utilisateurVo = gson.fromJson(utilisateur, UtilisateurVO.class);
		
		return alerteService.getByUtilisateur(utilisateurVo);
	}
	
	/**
	 * Enregistre une nouvelle alerte
	 * 
	 * @param alerteVO
	 */
	@RequestMapping(value = "/alerte/add", method = RequestMethod.POST)
	public void add(@RequestBody AlerteVO alerteVO) {
		alerteService.add(alerteVO);
	}
	
	/**
	 * @param alerteVO
	 */
	@RequestMapping(value = "/alerte/update", method = RequestMethod.POST)
	public void update(@RequestBody AlerteVO alerteVO) {
		alerteService.update(alerteVO);
	}
	
	/**
	 * @param utilisateur
	 * @return la List des alertes actif de l'utilisateur
	 */
	@RequestMapping(value = "/alertes/user/actif", method = RequestMethod.GET)
	public List<AlerteVO> getByUtilisateurActif(@RequestParam("utilisateur") String utilisateur) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		UtilisateurVO utilisateurVo = gson.fromJson(utilisateur, UtilisateurVO.class);
		
		return alerteService.getByUtilisateurActif(utilisateurVo);
	}
	
	/**
	 * Suppression d'une alerte	
	 * 
	 * @param alerteVO
	 */
	@RequestMapping(value = "/alerte/delete", method = RequestMethod.POST)
	public void delete(@RequestBody AlerteVO alerteVO) {
		alerteService.delete(alerteVO);
	}

}
