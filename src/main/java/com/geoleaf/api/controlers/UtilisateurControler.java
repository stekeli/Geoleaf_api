package com.geoleaf.api.controlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geoleaf.api.beans.web.BadgeVO;
import com.geoleaf.api.beans.web.UtilisateurBadgeVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.services.IUtilisateurBadgeService;
import com.geoleaf.api.services.IUtilisateurService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class UtilisateurControler {

	@Autowired
	private IUtilisateurService utilisateurService;

	/**
	 * @return List de tous les utilisateurs
	 */
	@RequestMapping(value = "/utilisateurs", method = RequestMethod.GET)
	public List<UtilisateurVO> getAll() {
		return utilisateurService.getAll();
	}

	/**
	 * @param id
	 * @return Utilisateur par son id
	 */
	@RequestMapping(value = "/utilisateur/id", method = RequestMethod.GET)
	public UtilisateurVO get(@RequestParam("id") Integer id) {
		return utilisateurService.get(id);
	}

	/**
	 * @param email
	 * @return Utilisateur par son mail
	 */
	@RequestMapping(value = "/utilisateur/email", method = RequestMethod.GET)
	public UtilisateurVO getByEmail(@RequestParam("email") String email) {
		return utilisateurService.getByEmail(email);
	}

	/**
	 * Test si le login / mdp sont valides
	 * 
	 * @param email
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/utilisateur/exist/email/password", method = RequestMethod.GET)
	public boolean isExistEmailPassword(@RequestParam("email") String email, @RequestParam("password") String password) {
		return utilisateurService.isExistByEmailPassword(email, password);
	}

	/**
	 * Test si email existe deja
	 * 
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/utilisateur/exist/email", method = RequestMethod.GET)
	public boolean isExistEmail(@RequestParam("email") String email) {
		return utilisateurService.isExistByEmail(email);
	}

	/**
	 * Test si pseudo existe deja
	 * 
	 * @param pseudo
	 * @return
	 */
	@RequestMapping(value = "/utilisateur/exist/pseudo", method = RequestMethod.GET)
	public boolean isExistPseudo(@RequestParam("pseudo") String pseudo) {
		return utilisateurService.isExistByPseudo(pseudo);
	}

	/**
	 * Ajoute un utilisateur
	 * 
	 * @param utilisateurVo
	 */
	@RequestMapping(value = "/utilisateur/add", method = RequestMethod.POST)
	public void add(@RequestBody UtilisateurVO utilisateurVo) {
		utilisateurService.add(utilisateurVo);
	}
	
	@RequestMapping(value = "/utilisateur/update", method = RequestMethod.POST)
	public void update(@RequestBody UtilisateurVO utilisateurVo) {
		utilisateurService.update(utilisateurVo);
	}


}
