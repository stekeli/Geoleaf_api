package com.geoleaf.api.controlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geoleaf.api.beans.web.EspeceVO;
import com.geoleaf.api.repositories.IEspeceRepository;
import com.geoleaf.api.services.IEspeceService;
import com.geoleaf.api.utils.ImageSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.codec.binary.Base64;

@RestController
public class EspeceControler {

	@Autowired
	private IEspeceService especeService;

	@Autowired
	IEspeceRepository especeRepository;
	
	/**
	 * @return {@link List} toutes les especes
	 */
	@RequestMapping(value = "/especes", method = RequestMethod.GET)
	public List<EspeceVO> getAll() {
		return especeService.getAll();
	}	
	
	/**
	 * @return {@link List} toutes les especes
	 */
	@RequestMapping(value = "/espece/withimage/id", method = RequestMethod.GET)
	public EspeceVO getWithImage(@RequestParam("id") Integer id) {
		return especeService.getWithImage(id);
	}

	/**
	 * @param id
	 * @return {@link List} EspeceVO selon id
	 */
	@RequestMapping(value = "/espece/id", method = RequestMethod.GET)
	public EspeceVO getById(@RequestParam("id") Integer id) {
		return especeService.getById(id);
	}

	/**
	 * @param nomFrancais
	 * @return {@link List} EspeceVO selon le nomFrancais
	 */
	@RequestMapping(value = "/espece/nom", method = RequestMethod.GET)
	public EspeceVO getByNomFrancais(@RequestParam("nomFrancais") String nomFrancais) {
		return especeService.getByNomFrancais(nomFrancais);
	}

	/**
	 * Enregistre une nouvelle espece
	 * 
	 * @param especeVO
	 */
	@RequestMapping(value = "/espece/add", method = RequestMethod.POST)
	public void add(@RequestBody EspeceVO especeVO) {
		especeService.add(especeVO);
	}

	@RequestMapping(value = "/espece/image", produces = MediaType.IMAGE_JPEG_VALUE, method = RequestMethod.GET)
	public String getImageWithMediaType(@RequestParam("espece") String espece) throws IOException {
		
		final Gson gson = new GsonBuilder().create();
		EspeceVO especeVO = gson.fromJson(espece, EspeceVO.class);
		String imagePath = especeVO.getImagePath();
		
		File file = new File(imagePath);
		
        return ImageSerializer.toBase64(file);
	}
	
}
