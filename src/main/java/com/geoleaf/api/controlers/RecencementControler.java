package com.geoleaf.api.controlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geoleaf.api.beans.data.RecensementDO;
import com.geoleaf.api.beans.web.EspeceVO;
import com.geoleaf.api.beans.web.RecensementVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.services.IRecensementService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Kevin Wallois
 *
 */
@RestController
public class RecencementControler {

	@Autowired
	private IRecensementService recensementService;

	/**
	 * @return {@link List} de tous les recensements
	 */
	@RequestMapping(value = "/recensements", method = RequestMethod.GET)
	public List<RecensementVO> getAll() {
		return recensementService.getAll();
	}

	/**
	 * @param id
	 * @return {@link RecensementDO} un recensement selon son id
	 */
	@RequestMapping(value = "/recensement/id", method = RequestMethod.GET)
	public RecensementVO get(@RequestParam("id") Integer id) {
		return recensementService.get(id);
	}
	
	/**
	 * @param especeVO
	 * @return {@link List} des recensements d'une espece
	 */
	@RequestMapping(value = "/recensement/espece", method = RequestMethod.GET)
	public List<RecensementVO> getByEspece(@RequestParam("espece") String espece) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		EspeceVO especeVO = gson.fromJson(espece, EspeceVO.class);
		
		return recensementService.getByEspece(especeVO);
	}

	/**
	 * @param idUtilisateur
	 * @return {@link List} de recencement d un utilisateur
	 */
	@RequestMapping(value = "/recensements/user", method = RequestMethod.GET)
	public List<RecensementVO> getByUtilisateur(@RequestParam("utilisateur") String utilisateur) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		UtilisateurVO utilisateurVo = gson.fromJson(utilisateur, UtilisateurVO.class);
		
		return recensementService.getByUtilisateur(utilisateurVo);
	}
	
	@RequestMapping(value = "/recensements/count/user", method = RequestMethod.GET)
	public Integer countByUtilisateur(@RequestParam("utilisateur") String utilisateur) {
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		UtilisateurVO utilisateurVo = gson.fromJson(utilisateur, UtilisateurVO.class);
		
		return recensementService.countByUtilisateur(utilisateurVo);
	}

	/**
	 * Enregistre un recencement
	 * 
	 * @param recensementVO
	 */
	@RequestMapping(value = "/recensement/add", method = RequestMethod.POST)
	public Integer add(@RequestBody RecensementVO recensementVO) {
		return recensementService.add(recensementVO);
	}

	// TODO : Ajout methode pour afficher les recensements selon un rayon en metre

}
