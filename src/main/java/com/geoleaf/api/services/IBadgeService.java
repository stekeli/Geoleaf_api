package com.geoleaf.api.services;

import java.util.List;

import com.geoleaf.api.beans.web.BadgeVO;
import com.geoleaf.api.beans.web.UtilisateurVO;

public interface IBadgeService {

	public List<BadgeVO> findAll();
	
	public BadgeVO findById(Integer id);
	
	public void add(BadgeVO badgeVO);
	
}
