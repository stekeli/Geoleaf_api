package com.geoleaf.api.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.UtilisateurBadgeDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.BadgeVO;
import com.geoleaf.api.beans.web.UtilisateurBadgeVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.repositories.IUtilisateurBadgeRepository;

@Component
@Transactional
public class UtilisateurBadgeService implements IUtilisateurBadgeService {

	@Autowired
	private IUtilisateurBadgeRepository utilisateurBadgeRepository;

	
	public UtilisateurBadgeVO map(UtilisateurBadgeDO utilisateurBadgeDO) {
		UtilisateurBadgeVO utilisateurBadgeVO = new UtilisateurBadgeVO();
		BadgeService badgeService = new BadgeService();
		UtilisateurService utilisateurService = new UtilisateurService();
		
		utilisateurBadgeVO.setId(utilisateurBadgeDO.getId());
		utilisateurBadgeVO.setBadge(badgeService.map(utilisateurBadgeDO.getBadge()));
		utilisateurBadgeVO.setUtilisateur(utilisateurService.map(utilisateurBadgeDO.getUtilisateur()));
		utilisateurBadgeVO.setDateObtention(utilisateurBadgeDO.getDateObtention());

		return utilisateurBadgeVO;
	}
	
	public UtilisateurBadgeDO map(UtilisateurBadgeVO utilisateurBadgeVO) {
		UtilisateurBadgeDO utilisateurBadgeDO = new UtilisateurBadgeDO();
		BadgeService badgeService = new BadgeService();
		UtilisateurService utilisateurService = new UtilisateurService();
		
		utilisateurBadgeDO.setId(utilisateurBadgeVO.getId());
		utilisateurBadgeDO.setBadge(badgeService.map(utilisateurBadgeVO.getBadge()));
		utilisateurBadgeDO.setUtilisateur(utilisateurService.map(utilisateurBadgeVO.getUtilisateur()));
		utilisateurBadgeDO.setDateObtention(utilisateurBadgeVO.getDateObtention());

		return utilisateurBadgeDO;
	}

	public List<UtilisateurBadgeVO> mapVO(List<UtilisateurBadgeDO> utilisateurBadgeDOs) {
		List<UtilisateurBadgeVO> utilisateurBadgeVOs = new ArrayList<>();
		for (UtilisateurBadgeDO utilisateurBadgeDO : utilisateurBadgeDOs) {
			utilisateurBadgeVOs.add(map(utilisateurBadgeDO));
		}
		return utilisateurBadgeVOs;
	}
	
	public List<UtilisateurBadgeDO> mapDO(List<UtilisateurBadgeVO> utilisateurBadgeVOs) {
		List<UtilisateurBadgeDO> utilisateurBadgeDOs = new ArrayList<>();
		for (UtilisateurBadgeVO utilisateurBadgeVO : utilisateurBadgeVOs) {
			utilisateurBadgeDOs.add(map(utilisateurBadgeVO));
		}
		return utilisateurBadgeDOs;
	}

	@Override
	@Transactional
	public void add(UtilisateurBadgeVO utilisateurBadgeVO) {
		utilisateurBadgeVO.setDateObtention(new Date());
		utilisateurBadgeRepository.save(map(utilisateurBadgeVO));
	}

	@Override
	public UtilisateurBadgeVO findById(Integer id) {
		return map(utilisateurBadgeRepository.findById(id));
	}

	@Override
	public List<UtilisateurBadgeVO> findByUtilisateur(UtilisateurVO utilisateurVO) {
		UtilisateurService utilisateurService = new UtilisateurService();
		UtilisateurDO utilisateurDO = utilisateurService.map(utilisateurVO);
		
		return mapVO(utilisateurBadgeRepository.findByUtilisateur(utilisateurDO));
	}

}
