package com.geoleaf.api.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.repositories.IUtilisateurRepository;

@Component
@Transactional
public class UtilisateurService implements IUtilisateurService {

	@Autowired
	private IUtilisateurRepository utilisateurRepository;	

	@Override
	public List<UtilisateurVO> getAll() {
		return mapVO(utilisateurRepository.findAll());
	}

	@Override
	public UtilisateurVO get(int id) {
		return map(utilisateurRepository.findById(id));
	}

	@Override
	public UtilisateurVO getByEmail(String email) {
		return map(utilisateurRepository.findByEmail(email));
	}

	@Override
	public boolean isExistByEmailPassword(String email, String password) {
		return utilisateurRepository.existsByEmailAndPassword(email, password);
	}

	@Override
	public boolean isExistByEmail(String email) {
		return utilisateurRepository.existsByEmail(email);
	}

	@Override
	public boolean isExistByPseudo(String pseudo) {
		return utilisateurRepository.existsByPseudo(pseudo);
	}

	@Override
	@Transactional
	public void add(UtilisateurVO utilisateurVo) {
		utilisateurVo.setDateInscription(new Date());
		UtilisateurDO utilisateurDo = map(utilisateurVo);

		utilisateurRepository.save(utilisateurDo);
	}
	
	@Override
	@Transactional
	public void update(UtilisateurVO utilisateurVo) {
		utilisateurRepository.save(map(utilisateurVo));
	}

	/**
	 * Transforme UtilisateurVO en UtilisateurDO
	 * 
	 * @param {@link
	 *            UtilisateurVO}
	 * @return {@link UtilisateurDO}
	 */
	@Override
	public UtilisateurDO map(UtilisateurVO utilisateurVO) {
		UtilisateurDO utilisateurDO = new UtilisateurDO();
	
		utilisateurDO.setId(utilisateurVO.getId());
		utilisateurDO.setPseudo(utilisateurVO.getPseudo());
		utilisateurDO.setEmail(utilisateurVO.getEmail());
		utilisateurDO.setPassword(utilisateurVO.getPassword());
		utilisateurDO.setDateInscription(utilisateurVO.getDateInscription());
		utilisateurDO.setNbEspecesReference(utilisateurVO.getNbEspeceReference());
		utilisateurDO.setImagePath(utilisateurVO.getImagePath());
		
		return utilisateurDO;
	}

	/**
	 * @param {@link
	 *            UtilisateurDO}
	 * @return {@link UtilisateurVO}
	 */
	public UtilisateurVO map(UtilisateurDO utilisateurDO) {
		UtilisateurVO utilisateurVO = new UtilisateurVO();
		
		utilisateurVO.setId(utilisateurDO.getId());
		utilisateurVO.setPseudo(utilisateurDO.getPseudo());
		utilisateurVO.setEmail(utilisateurDO.getEmail());
		utilisateurVO.setPassword(utilisateurDO.getPassword());
		utilisateurVO.setDateInscription(utilisateurDO.getDateInscription());
		utilisateurVO.setNbEspeceReference(utilisateurDO.getNbEspecesReference());
		utilisateurVO.setImagePath(utilisateurDO.getImagePath());

		return utilisateurVO;
	}

	/**
	 * @param {@link
	 *            List} de {@link UtilisateurDO}
	 * @return {@link List} de {@link UtilisateurVO}
	 */
	private List<UtilisateurVO> mapVO(List<UtilisateurDO> utilisateurDOs) {
		List<UtilisateurVO> utilisateurVOs = new ArrayList<>();
		for (UtilisateurDO utilisateurDO : utilisateurDOs) {
			utilisateurVOs.add(map(utilisateurDO));
		}
		return utilisateurVOs;
	}

	/**
	 * @param {@link
	 *            List} de {@link UtilisateurDO}
	 * @return {@link List} de {@link UtilisateurVO}
	 */
	private List<UtilisateurDO> mapDO(List<UtilisateurVO> utilisateurVOs) {
		List<UtilisateurDO> utilisateurDOs = new ArrayList<>();
		for (UtilisateurVO utilisateurVO : utilisateurVOs) {
			utilisateurDOs.add(map(utilisateurVO));
		}
		return utilisateurDOs;
	}

}
