package com.geoleaf.api.services;

import java.io.IOException;
import java.util.List;

import com.geoleaf.api.beans.data.EspeceDO;
import com.geoleaf.api.beans.web.EspeceVO;

public interface IEspeceService {

	public List<EspeceVO> getAll();
	
	public EspeceVO getById(int id);
	
	public EspeceVO getByNomFrancais(String nom);
	
	public void add(EspeceVO especeVO);
	
	public EspeceDO map(EspeceVO especeVO);

	public EspeceVO getWithImage(Integer id);
}
