package com.geoleaf.api.services;

import java.util.List;

import com.geoleaf.api.beans.web.BadgeVO;
import com.geoleaf.api.beans.web.UtilisateurBadgeVO;
import com.geoleaf.api.beans.web.UtilisateurVO;

public interface IUtilisateurBadgeService {
	
	public UtilisateurBadgeVO findById(Integer id);

	public List<UtilisateurBadgeVO> findByUtilisateur(UtilisateurVO utilisateurVO);
	
	public void add(UtilisateurBadgeVO utilisateurBadgeVO);
	
}
