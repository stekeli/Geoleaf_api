package com.geoleaf.api.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.BadgeDO;
import com.geoleaf.api.beans.web.BadgeVO;
import com.geoleaf.api.repositories.IBadgeRepository;

@Component
@Transactional
public class BadgeService implements IBadgeService {

	@Autowired
	private IBadgeRepository badgeRepository;

	@Override
	public List<BadgeVO> findAll() {
		return mapVO(badgeRepository.findAll());
	}

	@Override
	public BadgeVO findById(Integer id) {
		return map(badgeRepository.findById(id));
	}

	/**
	 * Transforme BadgeDO en BadgeVO
	 * 
	 * @param badgeDO
	 * @return
	 */
	public BadgeVO map(BadgeDO badgeDO) {
		BadgeVO badgeVO = new BadgeVO();

		badgeVO.setId(badgeDO.getId());
		badgeVO.setDescription(badgeDO.getDescription());
		badgeVO.setImagePath(badgeDO.getImagePath());
		badgeVO.setLibelle(badgeDO.getLibelle());
		badgeVO.setType(badgeDO.getType());
		badgeVO.setNombre(badgeDO.getNombre());

		return badgeVO;
	}

	/**
	 * Transforme BadgeVO en BadgeDO
	 * 
	 * @param badgeVO
	 * @return
	 */
	public BadgeDO map(BadgeVO badgeVO) {
		BadgeDO badgeDO = new BadgeDO();

		badgeDO.setId(badgeVO.getId());
		badgeDO.setDescription(badgeVO.getDescription());
		badgeDO.setImagePath(badgeVO.getImagePath());
		badgeDO.setLibelle(badgeVO.getLibelle());
		badgeDO.setType(badgeVO.getType());
		badgeDO.setNombre(badgeVO.getNombre());

		return badgeDO;
	}

	/**
	 * Transforme la Liste BadgeDO en Liste de BadgeVO
	 * 
	 * @param badgeDo
	 * @return
	 */
	public List<BadgeVO> mapVO(Collection<BadgeDO> badgeDo) {
		List<BadgeVO> badgeVO = new ArrayList<>();
		for (BadgeDO badge : badgeDo) {
			badgeVO.add(map(badge));
		}
		return badgeVO;
	}
	

	/**
	 * Transforme la Liste BadgeVO en Liste de BadgeDO
	 * 
	 * @param badgeVo
	 * @return
	 */
	public List<BadgeDO> mapDO(List<BadgeVO> badgeVo) {
		List<BadgeDO> badgeDO = new ArrayList<>();
		for (BadgeVO badge : badgeVo) {
			badgeDO.add(map(badge));
		}
		return badgeDO;
	}

	/*
	 * Enregistre un nouveau badge
	 * 
	 * @see com.geoleaf.api.services.IBadgeService#add(com.geoleaf.api.beans.web.BadgeVO)
	 */
	@Override
	@Transactional
	public void add(BadgeVO badgeVO) {
		BadgeDO badgeDO = map(badgeVO);
		badgeRepository.save(badgeDO);
	}

}
