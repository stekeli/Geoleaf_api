package com.geoleaf.api.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.EspeceDO;
import com.geoleaf.api.beans.data.RecensementDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.EspeceVO;
import com.geoleaf.api.beans.web.RecensementVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.repositories.IRecensementRepository;
import com.geoleaf.api.repositories.IUtilisateurRepository;

/**
 * @author Kevin Wallois
 *
 */
@Component
@Transactional
public class RecensementService implements IRecensementService {

	@Autowired
	private IRecensementRepository recensementRepository;

	@Autowired
	private IUtilisateurRepository utilisateurRepository;

	@Override
	public List<RecensementVO> getAll() {
		return mapVO(recensementRepository.findAll());
	}
	
	/* Recherche des recensements par le nom de l'espece
	 * @see com.geoleaf.api.services.IRecensementService#getByEspece(com.geoleaf.api.beans.web.EspeceVO)
	 */
	@Override
	public List<RecensementVO> getByEspece(EspeceVO espece) {
		IEspeceService especeService = new EspeceService();
		EspeceDO especeDO = especeService.map(espece);
		return mapVO(recensementRepository.findByEspece(especeDO));
	}

	@Override
	public RecensementVO get(int id) {
		return map(recensementRepository.findById(id));
	}

	@Override
	public List<RecensementVO> getByUtilisateur(UtilisateurVO utilisateurVo) {
		IUtilisateurService utilisateurService = new UtilisateurService();
		UtilisateurDO utilisateurDO = utilisateurService.map(utilisateurVo);
		return mapVO(recensementRepository.findByUtilisateur(utilisateurDO));
	}
	

	@Override
	public Integer countByUtilisateur(UtilisateurVO utilisateur) {
		IUtilisateurService utilisateurService = new UtilisateurService();
		return recensementRepository.countByUtilisateur(utilisateurService.map(utilisateur));
	}

	/**
	 * Map un RecensementVO en RecensementDO
	 * 
	 * @param recensementVO
	 * @return
	 */
	public RecensementDO map(RecensementVO recensementVO) {

		UtilisateurService utilisateur = new UtilisateurService();
		EspeceService espece = new EspeceService();
		VoteService voteService = new VoteService();
		RecensementDO recensementDO = new RecensementDO();

		recensementDO.setId(recensementVO.getId());
		recensementDO.setDateRecensement(recensementVO.getDateRecensement());
		recensementDO.setGpsLatitude(recensementVO.getGpsLatitude());
		recensementDO.setGpsLongitude(recensementVO.getGpsLongitude());
		recensementDO.setImagePath(recensementVO.getImagePath());
		recensementDO.setEspece(espece.map(recensementVO.getEspece()));
		recensementDO.setUtilisateur(utilisateur.map(recensementVO.getUtilisateur()));

		return recensementDO;
	}

	/**
	 * Map un RecensementDO en RecensementVO
	 * 
	 * @param recensementDO
	 * @return
	 */
	public RecensementVO map(RecensementDO recensementDO) {

		UtilisateurService utilisateur = new UtilisateurService();
		EspeceService espece = new EspeceService();
		VoteService voteService = new VoteService();
		RecensementVO recensementVO = new RecensementVO();

		recensementVO.setId(recensementDO.getId());
		recensementVO.setDateRecensement(recensementDO.getDateRecensement());
		recensementVO.setGpsLatitude(recensementDO.getGpsLatitude());
		recensementVO.setGpsLongitude(recensementDO.getGpsLongitude());
		recensementVO.setImagePath(recensementDO.getImagePath());
		recensementVO.setEspece(espece.map(recensementDO.getEspece()));
		recensementVO.setUtilisateur(utilisateur.map(recensementDO.getUtilisateur()));

		return recensementVO;
	}

	/**
	 * Transforme la Liste RecensementDO en ListRecensementVO
	 * 
	 * @param recensementDo
	 * @return
	 */
	public List<RecensementVO> mapVO(Collection<RecensementDO> recensementDo) {
		List<RecensementVO> recensementVO = new ArrayList<>();
		for (RecensementDO recensement : recensementDo) {
			recensementVO.add(map(recensement));
		}
		return recensementVO;
	}

	/**
	 * Transforme la Liste RecensementVO en ListRecensementDO
	 * 
	 * @param recensementVo
	 * @return
	 */
	public List<RecensementDO> mapDO(List<RecensementVO> recensementVo) {
		List<RecensementDO> recensementDo = new ArrayList<>();
		for (RecensementVO recensement : recensementVo) {
			recensementDo.add(map(recensement));
		}
		return recensementDo;
	}

	/*
	 * Enregistre un recensement
	 * 
	 * @see com.geoleaf.api.services.IRecensementService#add(com.geoleaf.api.beans.web.RecensementVO)
	 */
	@Override
	@Transactional
	public Integer add(RecensementVO recensementVO) {
		recensementVO.setDateRecensement(new Date());

		RecensementDO recensementDO = map(recensementVO);
		UtilisateurDO utilisateurDO = utilisateurRepository.findById(recensementDO.getUtilisateur().getId());
		utilisateurDO.setNbEspecesReference(utilisateurDO.getNbEspecesReference()+1);
		
		utilisateurRepository.save(utilisateurDO);
		
		RecensementDO recensement = recensementRepository.save(recensementDO);
		
		return map(recensement).getId();
	}

}
