package com.geoleaf.api.services;

import java.util.List;

import com.geoleaf.api.beans.web.EspeceVO;
import com.geoleaf.api.beans.web.RecensementVO;
import com.geoleaf.api.beans.web.UtilisateurVO;

public interface IRecensementService {

	public List<RecensementVO> getAll();

	public RecensementVO get(int id);

	public List<RecensementVO> getByUtilisateur(UtilisateurVO utilisateur);
	
	public Integer countByUtilisateur(UtilisateurVO utilisateur);
	
	public Integer add(RecensementVO recensementVO);
	
	public List<RecensementVO> getByEspece(EspeceVO espece);
}
