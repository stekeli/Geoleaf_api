package com.geoleaf.api.services;

import java.util.List;

import com.geoleaf.api.beans.web.AlerteVO;
import com.geoleaf.api.beans.web.UtilisateurVO;


public interface IAlerteService {

	public AlerteVO findById(Integer id);

	public List<AlerteVO> findAll();
	
	public List<AlerteVO> getByUtilisateur(UtilisateurVO utilisateur);
	
	public void add(AlerteVO alerteVO);
	
	public void update(AlerteVO alerteVO);
	
	public List<AlerteVO> getByUtilisateurActif(UtilisateurVO utilisateur);
	
	public void delete(AlerteVO alerte);
}
