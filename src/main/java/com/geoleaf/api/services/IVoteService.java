package com.geoleaf.api.services;

import java.util.List;

import com.geoleaf.api.beans.data.RecensementDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.RecensementVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.beans.web.VoteVO;

public interface IVoteService {

	public List<VoteVO> getAll();
	
	public VoteVO get(int id);
	
	public int getVotedValue(UtilisateurVO utilisateur, RecensementVO recensement);
	
	public Integer countPostiveVotesForRecensement(RecensementVO recensement);
	public Integer countNegativesVotesForRecensement(RecensementVO recensement);
	
	public VoteVO findByUtilisateur(UtilisateurVO utilisateur);
	
	public void add(VoteVO vote);

}
