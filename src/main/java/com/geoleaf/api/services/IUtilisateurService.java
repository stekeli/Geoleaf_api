package com.geoleaf.api.services;

import java.util.List;

import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.UtilisateurVO;

public interface IUtilisateurService {

	public List<UtilisateurVO> getAll();
	
	public UtilisateurVO get(int id);
	
	public boolean isExistByEmailPassword(String email, String password);
	
	public boolean isExistByEmail(String email);
	
	public boolean isExistByPseudo(String pseudo);
	
	public UtilisateurDO map(UtilisateurVO utilisateurVo);
	
	public void add(UtilisateurVO utilisateurVo);

	public UtilisateurVO getByEmail(String email);

	void update(UtilisateurVO utilisateurVo);
	
}
