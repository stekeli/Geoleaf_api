package com.geoleaf.api.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.RecensementDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.data.VoteDO;
import com.geoleaf.api.beans.web.RecensementVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.beans.web.VoteVO;
import com.geoleaf.api.repositories.IRecensementRepository;
import com.geoleaf.api.repositories.IVoteRepository;

/**
 * @author Kevin Wallois
 *
 */
@Component
@Transactional
public class VoteService implements IVoteService {

	@Autowired
	private IRecensementRepository recensementRepository;
	
	@Autowired
	private IVoteRepository voteRepository;

	@Override
	public List<VoteVO> getAll() {
		return mapVo(voteRepository.findAll());
	}

	@Override
	public VoteVO get(int id) {
		return map(voteRepository.findById(id));
	}

	// Retourne 1 ou -1 en fonction du vote. Si le vote n'existe pas, retourne 0.
	@Override
	public int getVotedValue(UtilisateurVO utilisateur, RecensementVO recensement) {
		UtilisateurService utilisateurService = new UtilisateurService();
		RecensementService recensementService = new RecensementService();

		UtilisateurDO utilisateurDO = utilisateurService.map(utilisateur);
		RecensementDO recensementDO = recensementService.map(recensement);
		
		if(voteRepository.existsByUtilisateurAndRecensement(utilisateurDO, recensementDO)) {
			VoteDO vote = voteRepository.findByUtilisateurAndRecensement(utilisateurDO, recensementDO);
			return vote.getNote();
		} else {
			return 0;
		}
	}

	@Override
	public VoteVO findByUtilisateur(UtilisateurVO utilisateur) {
		UtilisateurService utilisateurService = new UtilisateurService();
		VoteService voteService = new VoteService();

		return voteService.map(voteRepository.findByUtilisateur(utilisateurService.map(utilisateur)));
	}
	
	@Override
	public Integer countPostiveVotesForRecensement(RecensementVO recensement) {
		RecensementService recensementService = new RecensementService();
		return voteRepository.countByRecensementAndNote(recensementService.map(recensement), 1);
	}

	@Override
	public Integer countNegativesVotesForRecensement(RecensementVO recensement) {
		RecensementService recensementService = new RecensementService();
		return voteRepository.countByRecensementAndNote(recensementService.map(recensement), -1);
	}	

	@Override
	public void add(VoteVO vote) {
		voteRepository.save(map(vote));
	}

	public VoteVO map(VoteDO voteDo) {
		UtilisateurService utilisateurService = new UtilisateurService();
		RecensementService recensementService = new RecensementService();
		
		VoteVO voteVo = new VoteVO();
		voteVo.setId(voteDo.getId());
		voteVo.setNote(voteDo.getNote());
		voteVo.setUtilisateur(utilisateurService.map(voteDo.getUtilisateur()));
		voteVo.setRecensement((recensementService.map(voteDo.getRecensement())));
		
		return voteVo;
	}

	public VoteDO map(VoteVO voteVo) {
		UtilisateurService utilisateurService = new UtilisateurService();
		RecensementService recensementService = new RecensementService();
		
		VoteDO voteDo = new VoteDO();
		voteDo.setId(voteVo.getId());
		voteDo.setNote(voteVo.getNote());
		voteDo.setUtilisateur(utilisateurService.map(voteVo.getUtilisateur()));
		voteDo.setRecensement(recensementService.map(voteVo.getRecensement()));

		return voteDo;
	}
	
	public List<VoteVO> mapVo(List<VoteDO> voteDos) {
		List<VoteVO> voteVos = new ArrayList<>();
		for (VoteDO voteDO : voteDos) {
			voteVos.add(map(voteDO));
		}
		return voteVos;
	}	
	
	public List<VoteDO> mapDo(List<VoteVO> voteVos) {
		List<VoteDO> voteDos = new ArrayList<>();
		for (VoteVO voteVO : voteVos) {
			voteDos.add(map(voteVO));
		}
		return voteDos;
	}

	
}
