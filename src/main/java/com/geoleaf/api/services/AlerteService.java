package com.geoleaf.api.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.AlerteDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.AlerteVO;
import com.geoleaf.api.beans.web.UtilisateurVO;
import com.geoleaf.api.repositories.IAlerteRepository;

@Component
@Transactional
public class AlerteService implements IAlerteService {

	@Autowired
	private IAlerteRepository alerteRepository;

	@Override
	public AlerteVO findById(Integer id) {
		return map(alerteRepository.findById(id));
	}

	@Override
	public List<AlerteVO> findAll() {
		return mapVO(alerteRepository.findAll());
	}

	/*
	 * List des alertes actif d'un utilisateur
	 * 
	 * @see com.geoleaf.api.services.IAlerteService#getByUtilisateurActif(com.geoleaf.api.beans.web.UtilisateurVO)
	 */
	@Override
	public List<AlerteVO> getByUtilisateurActif(UtilisateurVO utilisateurVO) {
		IUtilisateurService utilisateurService = new UtilisateurService();
		UtilisateurDO utilisateurDO = utilisateurService.map(utilisateurVO);

		return mapVO(alerteRepository.findByUtilisateurAndActifTrue(utilisateurDO));

	}

	@Override
	public List<AlerteVO> getByUtilisateur(UtilisateurVO utilisateurVO) {
		IUtilisateurService utilisateurService = new UtilisateurService();
		UtilisateurDO utilisateurDO = utilisateurService.map(utilisateurVO);

		return mapVO(alerteRepository.findByUtilisateur(utilisateurDO));
	}

	/**
	 * Transforme une AlerteDO en AlerteVO
	 * 
	 * @param alerteDO
	 * @return
	 */
	public AlerteVO map(AlerteDO alerteDO) {
		AlerteVO alerteVO = new AlerteVO();
		EspeceService espece = new EspeceService();
		UtilisateurService utilisateur = new UtilisateurService();

		alerteVO.setId(alerteDO.getId());
		alerteVO.setLibelle(alerteDO.getLibelle());
		alerteVO.setRayon(alerteDO.getRayon());
		alerteVO.setActif(alerteDO.getActif());
		alerteVO.setEspece(espece.map(alerteDO.getEspece()));
		alerteVO.setUtilisateur(utilisateur.map(alerteDO.getUtilisateur()));

		return alerteVO;
	}

	/**
	 * Transforme une AlerteVO en AlerteDO
	 * 
	 * @param alerteVO
	 * @return
	 */
	public AlerteDO map(AlerteVO alerteVO) {
		AlerteDO alerteDO = new AlerteDO();
		EspeceService espece = new EspeceService();
		UtilisateurService utilisateur = new UtilisateurService();

		alerteDO.setId(alerteVO.getId());
		alerteDO.setLibelle(alerteVO.getLibelle());
		alerteDO.setRayon(alerteVO.getRayon());
		alerteDO.setActif(alerteVO.getActif());
		alerteDO.setEspece(espece.map(alerteVO.getEspece()));
		alerteDO.setUtilisateur(utilisateur.map(alerteVO.getUtilisateur()));

		return alerteDO;
	}

	/**
	 * @param alerteDO
	 * @return
	 */
	public List<AlerteVO> mapVO(Collection<AlerteDO> alerteDO) {
		List<AlerteVO> alerteVO = new ArrayList<>();
		for (AlerteDO alerte : alerteDO) {
			alerteVO.add(map(alerte));
		}
		return alerteVO;
	}

	/**
	 * @param alerteVO
	 * @return
	 */
	public List<AlerteDO> mapDO(List<AlerteVO> alerteVO) {
		List<AlerteDO> alerteDO = new ArrayList<>();
		for (AlerteVO alerte : alerteVO) {
			alerteDO.add(map(alerte));
		}
		return alerteDO;
	}

	/*
	 * Enregistrement d'une alerte
	 * 
	 * @see com.geoleaf.api.services.IAlerteService#add(com.geoleaf.api.beans.web.AlerteVO)
	 */
	@Override
	@Transactional
	public void add(AlerteVO alerteVO) {
		AlerteDO alerteDo = map(alerteVO);
		alerteRepository.save(alerteDo);
	}

	/*
	 * Update d'une alerte
	 * 
	 * @see com.geoleaf.api.services.IAlerteService#update(com.geoleaf.api.beans.web.AlerteVO)
	 */
	@Override
	@Transactional
	public void update(AlerteVO alerteVO) {
		alerteRepository.save(map(alerteVO));
	}

	/*
	 * Suppression d'une alerte
	 * 
	 * @see com.geoleaf.api.services.IAlerteService#delete(com.geoleaf.api.beans.web.AlerteVO)
	 */
	@Override
	@Transactional
	public void delete(AlerteVO alerteVO) {
		alerteRepository.delete(map(alerteVO));
	}

}
