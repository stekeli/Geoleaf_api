package com.geoleaf.api.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geoleaf.api.beans.data.EspeceDO;
import com.geoleaf.api.beans.web.EspeceVO;
import com.geoleaf.api.repositories.IEspeceRepository;
import com.geoleaf.api.utils.ImageSerializer;

/**
 * @author Kevin Wallois
 *
 */

@Component
@Transactional
public class EspeceService implements IEspeceService {

	@Autowired
	private IEspeceRepository especeRepository;

	/**
	 * Transforme EspeceVO en EspeceDO
	 * 
	 * @param especeDO
	 * @return
	 */
	public EspeceDO map(EspeceVO especeVO) {
		EspeceDO especeDO = new EspeceDO();

		especeDO.setId(especeVO.getId());
		especeDO.setClimat(especeVO.getClimat());
		especeDO.setDescription(especeVO.getDescription());
		especeDO.setNomFrancais(especeVO.getNomFrancais());
		especeDO.setNomScientifique(especeVO.getNomScientifique());
		especeDO.setPhoto(especeVO.getImagePath());
		especeDO.setLien(especeVO.getLien());

		return especeDO;
	}

	/**
	 * Transforme EspeceDO en EspeceVO
	 * 
	 * @param especeDO
	 * @return
	 */
	public EspeceVO map(EspeceDO especeDO) {
		EspeceVO especeVO = new EspeceVO();

		especeVO.setId(especeDO.getId());
		especeVO.setClimat(especeDO.getClimat());
		especeVO.setDescription(especeDO.getDescription());
		especeVO.setNomFrancais(especeDO.getNomFrancais());
		especeVO.setNomScientifique(especeDO.getNomScientifique());
		especeVO.setImagePath(especeDO.getPhoto());
		especeVO.setLien(especeDO.getLien());

		return especeVO;
	}

	/**
	 * Transforme la Liste EspeceDO en Liste de EspeceVO
	 * 
	 * @param especeDo
	 * @return
	 */
	private List<EspeceVO> mapVO(List<EspeceDO> especeDo) {
		List<EspeceVO> especeVO = new ArrayList<>();
		for (EspeceDO espece : especeDo) {
			especeVO.add(map(espece));
		}
		return especeVO;
	}

	/*
	 * Return la liste des EspeceVO
	 * 
	 * @see com.geoleaf.api.services.IEspeceService#getAll()
	 */
	@Override
	public List<EspeceVO> getAll() {
		return mapVO(especeRepository.findAll());
	}

	/*
	 * Return la liste des EspeceVO avec leurs images (Base64) à la place des liens
	 * 
	 * @see com.geoleaf.api.services.IEspeceService#getAll()
	 */
	@Override
	public EspeceVO getWithImage(Integer id) {
		EspeceVO espece = map(especeRepository.findById(id));
	
		File file = new File(espece.getImagePath());
		espece.setImagePath(ImageSerializer.toBase64(file));
		
		return espece;
	}	
	
	/*
	 * Return une especeVo selon un id
	 * 
	 * @see com.geoleaf.api.services.IEspeceService#getById(int)
	 */
	@Override
	public EspeceVO getById(int id) {
		return map(especeRepository.findById(id));
	}

	/*
	 * Return une especeVo selon le nom Francais de l'espece
	 * 
	 * @see com.geoleaf.api.services.IEspeceService#getByNomFrancais(java.lang.String)
	 */
	@Override
	public EspeceVO getByNomFrancais(String nomFrancais) {
		return map(especeRepository.findByNomFrancais(nomFrancais));
	}

	/*
	 * Enregistrement d'une nouvelle espece
	 * 
	 * @see com.geoleaf.api.services.IEspeceService#add(com.geoleaf.api.beans.web.EspeceVO)
	 */
	@Override
	@Transactional
	public void add(EspeceVO especeVO) {
		EspeceDO especeDo = map(especeVO);
		especeRepository.save(especeDo);
	}

}
