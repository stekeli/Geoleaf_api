package com.geoleaf.api.beans.web;

import java.util.Date;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class UtilisateurVO {
    private Integer id;
    private String pseudo;
	private String email;
    private String password;
    private Date dateInscription;
    private int nbEspeceReference;
    private String imagePath;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}
	public int getNbEspeceReference() {
		return nbEspeceReference;
	}
	public void setNbEspeceReference(int nbEspeceReference) {
		this.nbEspeceReference = nbEspeceReference;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}
