package com.geoleaf.api.beans.web;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class AlerteVO {
    private Integer id;
    private String libelle;
    private int rayon;
    private EspeceVO espece;
    private UtilisateurVO utilisateur;
    private Boolean actif;
    
    public UtilisateurVO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurVO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getRayon() {
        return rayon;
    }

    public void setRayon(int rayon) {
        this.rayon = rayon;
    }

    public EspeceVO getEspece() {
        return espece;
    }

    public void setEspece(EspeceVO espece) {
        this.espece = espece;
    }
    
	public Boolean getActif() {
		return actif;
	}

	public void setActif(Boolean actif) {
		this.actif = actif;
	}
}
