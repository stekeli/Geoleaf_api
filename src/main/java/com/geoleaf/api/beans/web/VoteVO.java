package com.geoleaf.api.beans.web;

public class VoteVO {
	
	private Integer id;
	private Integer note; // 1 ou -1
	private UtilisateurVO utilisateur;
	private RecensementVO recensement;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNote() {
		return note;
	}
	public void setNote(Integer note) {
		this.note = note;
	}
	public UtilisateurVO getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(UtilisateurVO utilisateur) {
		this.utilisateur = utilisateur;
	}
	public RecensementVO getRecensement() {
		return recensement;
	}
	public void setRecensement(RecensementVO recensement) {
		this.recensement = recensement;
	}

}



	

