package com.geoleaf.api.beans.web;

import java.util.Date;
import java.util.List;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class RecensementVO {
    private Integer id;
    private EspeceVO espece;
    private UtilisateurVO utilisateur;
    private Date DateRecensement;
    private Double gpsLatitude;
    private Double gpsLongitude;
    private String imagePath;
    private List<VoteVO> votes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspeceVO getEspece() {
        return espece;
    }

    public void setEspece(EspeceVO espece) {
        this.espece = espece;
    }

    public UtilisateurVO getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(UtilisateurVO utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Date getDateRecensement() {
        return DateRecensement;
    }

    public void setDateRecensement(Date dateRecensement) {
        DateRecensement = dateRecensement;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

	public List<VoteVO> getVotes() {
		return votes;
	}

	public void setVotes(List<VoteVO> votes) {
		this.votes = votes;
	}
    
}
