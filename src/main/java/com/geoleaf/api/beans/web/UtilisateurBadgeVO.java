package com.geoleaf.api.beans.web;

import java.io.Serializable;
import java.util.Date;

public class UtilisateurBadgeVO implements Serializable {
	
	private Integer id;
	private UtilisateurVO utilisateur;
	private BadgeVO badge;
	private Date dateObtention;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UtilisateurVO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurVO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public BadgeVO getBadge() {
		return badge;
	}

	public void setBadge(BadgeVO badge) {
		this.badge = badge;
	}

	public Date getDateObtention() {
		return dateObtention;
	}

	public void setDateObtention(Date dateObtention) {
		this.dateObtention = dateObtention;
	}
	
}






	

