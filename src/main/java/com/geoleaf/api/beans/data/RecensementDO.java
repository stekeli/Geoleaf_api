package com.geoleaf.api.beans.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Recensement")
public class RecensementDO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@ManyToOne(targetEntity=EspeceDO.class)
	private EspeceDO espece;
	
	@JsonIgnoreProperties({"pseudo", "email"})
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = UtilisateurDO.class)
    @JoinColumn(name = "utilisateur_id")
	private UtilisateurDO utilisateur;
	
	@Temporal(TemporalType.DATE)
	@Column(name="date_recensement", nullable=false)
	private Date dateRecensement;
	
	@Column(name="gps_latitude", nullable=false)
	private Double gpsLatitude;
	
	@Column(name="gps_longitude", nullable=false)
	private Double gpsLongitude;
	
	@Column(name="image_path")
	private String imagePath;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EspeceDO getEspece() {
		return espece;
	}

	public void setEspece(EspeceDO espece) {
		this.espece = espece;
	}

	public UtilisateurDO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDO utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	public Date getDateRecensement() {
		return dateRecensement;
	}

	public void setDateRecensement(Date dateRecensement) {
		this.dateRecensement = dateRecensement;
	}

	public Double getGpsLatitude() {
		return gpsLatitude;
	}

	public void setGpsLatitude(Double gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public Double getGpsLongitude() {
		return gpsLongitude;
	}

	public void setGpsLongitude(Double gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

}






	

