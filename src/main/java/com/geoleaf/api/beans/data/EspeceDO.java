package com.geoleaf.api.beans.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Espece")
public class EspeceDO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;

	@Column(name="nom_francais", nullable=false)
	private String nomFrancais;

	@Column(name="nom_scientifique")
	private String nomScientifique;

	@Column(name="description", nullable=false)
	private String description;

	@Column(name="climat")
	private String climat;

	@Column(name="photo", nullable=false)
	private String photo;

	@Column(name="lien", nullable=false)
	private String lien;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomFrancais() {
		return nomFrancais;
	}

	public void setNomFrancais(String nomFrancais) {
		this.nomFrancais = nomFrancais;
	}

	public String getNomScientifique() {
		return nomScientifique;
	}

	public void setNomScientifique(String nomScientifique) {
		this.nomScientifique = nomScientifique;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getClimat() {
		return climat;
	}

	public void setClimat(String climat) {
		this.climat = climat;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getLien() {
		return lien;
	}

	public void setLien(String lien) {
		this.lien = lien;
	}
	
}






	

