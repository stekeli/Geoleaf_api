package com.geoleaf.api.beans.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Alerte")
public class AlerteDO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "libelle", nullable = false)
	private String libelle;

	@Column(name = "rayon", nullable = false)
	private Integer rayon;

	@ManyToOne(targetEntity = EspeceDO.class)
	private EspeceDO espece;

	@JsonIgnoreProperties("alertes")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "utilisateur_id")
	private UtilisateurDO utilisateur;

	@Column(name = "actif", nullable = false)
	private Boolean actif;

	public UtilisateurDO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getRayon() {
		return rayon;
	}

	public void setRayon(Integer rayon) {
		this.rayon = rayon;
	}

	public EspeceDO getEspece() {
		return espece;
	}

	public void setEspece(EspeceDO espece) {
		this.espece = espece;
	}

	public Boolean getActif() {
		return actif;
	}

	public void setActif(Boolean actif) {
		this.actif = actif;
	}

}
