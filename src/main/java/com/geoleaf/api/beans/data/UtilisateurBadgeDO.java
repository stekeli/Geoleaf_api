package com.geoleaf.api.beans.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="UtilisateurBadge")
public class UtilisateurBadgeDO implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@ManyToOne(targetEntity=UtilisateurDO.class)
	private UtilisateurDO utilisateur;
	
	@ManyToOne(targetEntity=BadgeDO.class)
	private BadgeDO badge;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_obtention", nullable = false)
	private Date dateObtention;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UtilisateurDO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public BadgeDO getBadge() {
		return badge;
	}

	public void setBadge(BadgeDO badge) {
		this.badge = badge;
	}

	public Date getDateObtention() {
		return dateObtention;
	}

	public void setDateObtention(Date dateObtention) {
		this.dateObtention = dateObtention;
	}
	
	
	
	
}






	

