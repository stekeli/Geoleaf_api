package com.geoleaf.api.beans.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Vote")
public class VoteDO implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;

	@Column(name="note", nullable=false)
	private Integer note; // 1 ou -1

	@JsonIgnoreProperties({"recensements", "badges", "alertes"})
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "utilisateur_id")
	private UtilisateurDO utilisateur;
	
	@ManyToOne(targetEntity=RecensementDO.class)
	private RecensementDO recensement;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNote() {
		return note;
	}

	public void setNote(Integer note) {
		this.note = note;
	}

	public UtilisateurDO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public RecensementDO getRecensement() {
		return recensement;
	}

	public void setRecensement(RecensementDO recensement) {
		this.recensement = recensement;
	}
	
}






	

