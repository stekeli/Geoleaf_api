package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.UtilisateurDO;

@Repository
@Transactional
public interface IUtilisateurRepository extends JpaRepository<UtilisateurDO, Serializable> {

	public UtilisateurDO findById(Integer id);

	public List<UtilisateurDO> findAll();
	
	public boolean existsByEmailAndPassword(String email, String password);
	
	public boolean existsByEmail(String email);
	
	public boolean existsByPseudo(String pseudo);
	
	public UtilisateurDO save(UtilisateurDO utilisateur);

	public UtilisateurDO findByEmail(String email);


}
