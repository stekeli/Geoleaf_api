package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.BadgeDO;
import com.geoleaf.api.beans.data.UtilisateurBadgeDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.web.UtilisateurBadgeVO;

@Repository
@Transactional
public interface IUtilisateurBadgeRepository extends JpaRepository<UtilisateurBadgeDO, Serializable> {

	public UtilisateurBadgeDO findById(Integer id);

	public List<UtilisateurBadgeDO> findByUtilisateur(UtilisateurDO utilisateurDO);
	
	public UtilisateurBadgeDO save(UtilisateurBadgeDO utilisateurBadgeDO);
	
}
