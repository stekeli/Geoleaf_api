package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.BadgeDO;

@Repository
@Transactional
public interface IBadgeRepository extends JpaRepository<BadgeDO, Serializable> {

	public BadgeDO findById(Integer id);

	public List<BadgeDO> findAll();
	
	public BadgeDO save(BadgeDO badgeDO);
}
