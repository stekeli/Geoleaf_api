package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.AlerteDO;
import com.geoleaf.api.beans.data.UtilisateurDO;

@Repository
@Transactional
public interface IAlerteRepository extends JpaRepository<AlerteDO, Serializable> {

	public AlerteDO findById(Integer id);

	public List<AlerteDO> findAll();

	public List<AlerteDO> findByUtilisateurAndActifTrue(UtilisateurDO utilisateur);
	
	public AlerteDO save(AlerteDO alerteDo);

	public List<AlerteDO> findByUtilisateur(UtilisateurDO utilisateur);
	
	public void deleteById(AlerteDO alerte);
}
