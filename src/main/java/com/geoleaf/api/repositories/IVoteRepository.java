package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.RecensementDO;
import com.geoleaf.api.beans.data.UtilisateurDO;
import com.geoleaf.api.beans.data.VoteDO;
import com.geoleaf.api.beans.web.UtilisateurVO;

@Repository
@Transactional
public interface IVoteRepository extends JpaRepository<VoteDO, Serializable> {

	public VoteDO findById(Integer id);
	
	public VoteDO findByUtilisateurAndRecensement(UtilisateurDO utilisateur, RecensementDO recensement);
	
	public Integer countByRecensementAndNote(RecensementDO recensement, Integer note);

	public List<VoteDO> findAll();
	
	public boolean existsByUtilisateurAndRecensement(UtilisateurDO utilisateur, RecensementDO recensement);
	
	public VoteDO save(VoteDO vote);

	public VoteDO findByUtilisateur(UtilisateurDO utilisateur);


}
