package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.EspeceDO;
import com.geoleaf.api.beans.data.RecensementDO;
import com.geoleaf.api.beans.data.UtilisateurDO;

@Repository
@Transactional
public interface IRecensementRepository extends JpaRepository<RecensementDO, Serializable> {
	
	public RecensementDO findById(Integer id);
	
	public List<RecensementDO> findAll();
	
	public List<RecensementDO> findByUtilisateur(UtilisateurDO utilisateur);
	
	public Integer countByUtilisateur(UtilisateurDO utilisateur);
	
	public List<RecensementDO> findByEspece(EspeceDO espece);
	
	public RecensementDO save(RecensementDO recensement);

}
