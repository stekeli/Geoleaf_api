package com.geoleaf.api.repositories;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.geoleaf.api.beans.data.EspeceDO;

@Repository
@Transactional
public interface IEspeceRepository extends JpaRepository<EspeceDO, Serializable> {

	public List<EspeceDO> findAll();

	public EspeceDO findById(Integer id);

	public EspeceDO findByNomFrancais(String nomFrancais);

	public EspeceDO save(EspeceDO especeDo);

}
